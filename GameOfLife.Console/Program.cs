﻿using GameOfLife.ConsoleApp.Renderers;
using GameOfLife.Engine;
using System;
using GameOfLife.Engine.SpaceProviders.Solid;
using GameOfLife.Engine.SpaceProviders.Sliced;
using GameOfLife.Engine.SpaceProviders.Sliced.ConfigurationProviders;
using GameOfLife.Engine.GameIterationCommands.Distributed;
using GameOfLife.Engine.GameIterationCommands.Distributed.EvolverProviders;
using GameOfLife.Engine.GameIterationCommands;
using GameOfLife.Engine.Evolvers;

namespace GameOfLife.ConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            var solidSpaceProvider = new SolidBinaryRandomSpaceProvider();

            #region Sync

            //var gameEvolver = new ConwaySyncronousEvolver();
            //var renderer = new ConsoleTwoDimensionalBinarRenderer();
            //var solidGameIterationCommand = new SolidSpaceGameIterationCommand<bool>(renderer, gameEvolver);

            //var engine = new GameFacade<bool[,]>(solidSpaceProvider, solidGameIterationCommand);

            #endregion

            #region Parallel
            //var renderer = new ConsoleTwoDimensionalBinarRenderer();
            //var gameEvolver = new ConwayParallelEvolver();
            //var solidSpaceGameIterationCommand = new SolidSpaceGameIterationCommand<bool>(renderer, gameEvolver);
            //var engine = new GameFacade<bool[,]>(solidSpaceProvider, solidSpaceGameIterationCommand);
            #endregion

            #region Distributed
            var slicingDegreeProvider = new AppConfigSlicingDegreeConfigurationProvider();
            var spaceProvider = new SlicedOverlappedBinaryRandomSpaceProvider(solidSpaceProvider, slicingDegreeProvider);
            var renderer = new ConsoleSlicedTwoDimensionalBinarRenderer();
            //var evolversProvider = new AppConfigConwaySyncrounousEvolversProvider();
            var evolversProvider = new AppConfigConwayDistributedEvolversProvider();
            var remoteSlicedOverlappedSpaceGameIterationCommand = new DistributedSlicedOverlappedSpaceGameIterationCommand<bool>(renderer, evolversProvider);
            var engine = new GameFacade<bool[][,]>(spaceProvider, remoteSlicedOverlappedSpaceGameIterationCommand);
            #endregion

            engine.StartGame(50);

            Console.ReadKey();
        }
    }
}
