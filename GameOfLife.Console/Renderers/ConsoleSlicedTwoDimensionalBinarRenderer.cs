﻿using System;
using System.Text;
using GameOfLife.Engine.Renderers.Core;

namespace GameOfLife.ConsoleApp.Renderers
{
    public class ConsoleSlicedTwoDimensionalBinarRenderer : IGameRenderer<bool[][,]>
    {
        public void Render(bool[][,] space)
        {
            var builder = new StringBuilder();
            var height = space[0].GetLength(0);

            for (int i = 0; i < height; i++)
            {
                for (int u = 0; u < space.Length; u++)
                {
                    for (int j = 0; j < space[u].GetLength(1); j++)
                    {
                        var targetChar = space[u][i, j] ? '\u2588' : ' ';
                        builder.Append(targetChar);
                        builder.Append(targetChar);
                    }
                }

                builder.Append('\n');
            }

            Console.SetCursorPosition(0, 0);
            Console.Write(builder.ToString());
        }
    }
}
