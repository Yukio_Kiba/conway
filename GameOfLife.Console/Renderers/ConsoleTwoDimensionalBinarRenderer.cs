﻿using GameOfLife.Engine.Renderers.Core;
using System;
using System.Text;

namespace GameOfLife.ConsoleApp.Renderers
{
    public class ConsoleTwoDimensionalBinarRenderer : IGameRenderer<bool[,]>
    {
        public void Render(bool[,] space)
        {
            var builder = new StringBuilder();

            for (int i = 0; i < space.GetLength(0); i++)
            {
                for (int j = 0; j < space.GetLength(1); j++)
                {
                    var targetChar = space[i, j] ? '\u2588' : ' ';
                    builder.Append(targetChar);
                    builder.Append(targetChar);
                }

                builder.Append('\n');
            }

            Console.SetCursorPosition(0, 0);
            Console.Write(builder.ToString());
        }
    }
}
