﻿using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using GameOfLife.Engine.Evolvers.Core;

namespace GameOfLife.Engine.Evolvers
{
    public class ConwayDistributedEvolver : IGameEvolver<bool[,]>
    {
        private readonly IPEndPoint _remoteEvolver;

        public ConwayDistributedEvolver(IPEndPoint remoteEvolver)
        {
            _remoteEvolver = remoteEvolver;
        }

        public bool[,] Evolve(bool[,] previousState)
        {
            using (var memoryStream = new MemoryStream())
            {
                var targetSlice = previousState;

                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, targetSlice);

                var serialized = memoryStream.ToArray();

                using (var tcpClient = new TcpClient())
                {
                    tcpClient.Connect(_remoteEvolver);

                    var networkStream = tcpClient.GetStream();
                    networkStream.Write(serialized, 0, serialized.Length);

                    var raw = (bool[,])binaryFormatter.Deserialize(networkStream);

                    return raw;
                }
            }
        }

    }
}
