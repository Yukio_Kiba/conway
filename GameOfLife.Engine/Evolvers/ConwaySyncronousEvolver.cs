﻿using GameOfLife.Engine.Evolvers.Core;

namespace GameOfLife.Engine.Evolvers
{
    public class ConwaySyncronousEvolver : IGameEvolver<bool[,]>
    {
        public bool[,] Evolve(bool[,] previousState)
        {
            var result = new bool[previousState.GetLength(0), previousState.GetLength(1)];

            for (int i = 0; i < previousState.GetLength(0); i++)
            {
                for (int j = 0; j < previousState.GetLength(1); j++)
                {
                    var curr = previousState[i, j];
                    var liveNeighborhoodCount = CountNeighborhoods(previousState, i, j);

                    var res = 
                        curr && (liveNeighborhoodCount == 2 || liveNeighborhoodCount == 3)
                        || !curr && liveNeighborhoodCount == 3;

                    result[i, j] = res;
                }
            }

            return result;
        }

        private int CountNeighborhoods(bool[,] gameSpace, int x, int y)
        {
            var result = 0;

            var xStartPoint = (x - 1) < 0 ? 0 : x - 1;
            var xEndPoint = (x + 1) > gameSpace.GetLength(0) - 1 ? gameSpace.GetLength(0) - 1 : (x + 1);

            var yStartPoint = (y - 1) < 0 ? 0 : (y - 1);
            var yEndPoint = (y + 1) > gameSpace.GetLength(1) - 1 ? gameSpace.GetLength(1) - 1 : (y + 1);

            for (int i = xStartPoint; i <= xEndPoint; i++)
            {
                for (int j = yStartPoint; j <= yEndPoint; j++)
                {
                    if (gameSpace[i, j])
                    {
                        result++;
                    }
                }
            }

            return (gameSpace[x, y]) ? --result : result;
        }
    }
}
