﻿namespace GameOfLife.Engine.Evolvers.Core
{
    public interface IGameEvolver<TSpace>
    {
        TSpace Evolve(TSpace previousState);
    }
}
