﻿using System;

namespace GameOfLife.Engine.Extensions
{
    public static class TwoDimensionalArrayExtensions
    {
        public static T[,] SubArray<T>(this T[,] values, int minRowInclusive, int maxRowInclusive, int minColInclusive, int maxColInclusive)
        {
            int height = maxRowInclusive - minRowInclusive + 1;
            int width = maxColInclusive - minColInclusive + 1;
            var result = new T[height, width];

            int totalColumnsCount = values.GetLength(1);
            int fromIndex = minRowInclusive * totalColumnsCount + minColInclusive;
            int toIndex = 0;
            for (int row = 0; row <= height - 1; row++)
            {
                Array.Copy(values, fromIndex, result, toIndex, width);
                fromIndex += totalColumnsCount;
                toIndex += width;
            }

            return result;
        }
    }
}
