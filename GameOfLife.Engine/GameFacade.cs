﻿using GameOfLife.Engine.GameIterationCommands.Core;
using GameOfLife.Engine.StateProviders.Core;
using System.Threading;

namespace GameOfLife.Engine
{
    public class GameFacade<TSpace>
    {
        private readonly IExecuteGameIterationCommand<TSpace> _gameIterationCommand;
        private readonly ISpaceProvider<TSpace> _spaceProvider;

        public GameFacade(
            ISpaceProvider<TSpace> spaceProvider
            , IExecuteGameIterationCommand<TSpace> gameIterationCommand)
        {
            _spaceProvider = spaceProvider;
            _gameIterationCommand = gameIterationCommand;
        }

        public void StartGame(int tickIntervalInMs)
        {
            var stateToHandle = _spaceProvider.GetSpace();

            while (true)
            {
                var newState = _gameIterationCommand.Execute(stateToHandle);
                stateToHandle = newState;

                Thread.Sleep(tickIntervalInMs);
            }
        }
    }
}
