﻿namespace GameOfLife.Engine.GameIterationCommands.Core
{
    public interface IExecuteGameIterationCommand<TSpace>
    {
        TSpace Execute(TSpace space);
    }
}
