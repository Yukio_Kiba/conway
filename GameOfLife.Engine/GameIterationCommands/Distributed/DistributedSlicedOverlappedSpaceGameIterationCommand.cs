﻿using System.Collections.Generic;
using System.Linq;
using GameOfLife.Engine.Renderers.Core;
using System.Threading.Tasks;
using GameOfLife.Engine.GameIterationCommands.Distributed.EvolverProviders;
using System.Collections.Concurrent;
using GameOfLife.Engine.Evolvers.Core;

namespace GameOfLife.Engine.GameIterationCommands.Distributed
{
    public class DistributedSlicedOverlappedSpaceGameIterationCommand<T> : SlicedOverlappedSpaceGameIterationCommand<T>
    {
        private readonly IEnumerable<IGameEvolver<T[,]>> _distributedEvolvers;

        public DistributedSlicedOverlappedSpaceGameIterationCommand(
            IGameRenderer<T[][,]> gameRenderer
            , IEvolversProvider<T> gameEvolverProvider) : base(gameRenderer)
        {
            _distributedEvolvers = gameEvolverProvider.GetEvolvers();
        }

        protected override T[][,] GetEvolvedSpace(T[][,] overlappedSpace)
        {
            var freeDistributedEvolvers = new ConcurrentQueue<IGameEvolver<T[,]>>(_distributedEvolvers);
            var remainingSlices = new Queue<T[,]>(overlappedSpace);

            var taskList = new LinkedList<Task<T[,]>>();

            while (remainingSlices.Count != 0)
            {
                IGameEvolver<T[,]> targetEvolver;
                if (!freeDistributedEvolvers.TryDequeue(out targetEvolver))
                {
                    continue;
                }

                var targetSlice = remainingSlices.Dequeue();
                var task = Task.Run(() =>
                {
                    var res = targetEvolver.Evolve(targetSlice);
                    freeDistributedEvolvers.Enqueue(targetEvolver);

                    return res;
                });

                taskList.AddLast(task);
            }

            var result = Task.WhenAll(taskList).Result;

            return result;
        }
    }
}
