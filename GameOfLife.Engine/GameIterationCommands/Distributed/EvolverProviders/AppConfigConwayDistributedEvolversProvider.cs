﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using GameOfLife.Engine.Evolvers.Core;
using GameOfLife.Engine.Evolvers;

namespace GameOfLife.Engine.GameIterationCommands.Distributed.EvolverProviders
{
    public class AppConfigConwayDistributedEvolversProvider : IEvolversProvider<bool>
    {
        public IEnumerable<IGameEvolver<bool[,]>> GetEvolvers()
        {
            var raw = ConfigurationManager.AppSettings["remoteEvolverAddresses"];
            var remoteEvlolvers = raw.Split(';').Select(address =>
            {
                var splited = address.Split(':');
                var ip = splited[0];
                var port = splited[1];

                return new IPEndPoint(IPAddress.Parse(ip), int.Parse(port));
            }).Select(x => new ConwayDistributedEvolver(x));

            return remoteEvlolvers;
        }
    }
}
