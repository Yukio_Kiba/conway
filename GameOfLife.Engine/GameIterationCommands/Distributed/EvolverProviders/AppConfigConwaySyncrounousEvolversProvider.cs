﻿using System.Collections.Generic;
using System.Linq;
using GameOfLife.Engine.Evolvers.Core;
using System.Configuration;
using GameOfLife.Engine.Evolvers;

namespace GameOfLife.Engine.GameIterationCommands.Distributed.EvolverProviders
{
    public class AppConfigConwaySyncrounousEvolversProvider : IEvolversProvider<bool>
    {
        public IEnumerable<IGameEvolver<bool[,]>> GetEvolvers()
        {
            var maxThreadCount = int.Parse(ConfigurationManager.AppSettings["maxThreadCount"]);
            var result = Enumerable.Range(0, maxThreadCount).Select(x =>
            {
                return new ConwaySyncronousEvolver();
            });

            return result;
        }
    }
}
