﻿using System.Collections.Generic;
using GameOfLife.Engine.Evolvers.Core;

namespace GameOfLife.Engine.GameIterationCommands.Distributed.EvolverProviders
{

    public interface IEvolversProvider<T>
    {
        IEnumerable<IGameEvolver<T[,]>> GetEvolvers();
    }
}
