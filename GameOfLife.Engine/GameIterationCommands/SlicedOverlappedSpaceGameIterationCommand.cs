﻿using System.Linq;
using GameOfLife.Engine.GameIterationCommands.Core;
using GameOfLife.Engine.Renderers.Core;
using GameOfLife.Engine.Extensions;

namespace GameOfLife.Engine.GameIterationCommands
{
    public abstract class SlicedOverlappedSpaceGameIterationCommand<T> : IExecuteGameIterationCommand<T[][,]>
    {
        private readonly IGameRenderer<T[][,]> _renderer;

        public SlicedOverlappedSpaceGameIterationCommand(
            IGameRenderer<T[][,]> gameRenderer)
        {
            _renderer = gameRenderer;
        }

        public T[][,] Execute(T[][,] overlappedSpace)
        {
            var result = GetEvolvedSpace(overlappedSpace);

            var sanitizedSpaceToRender = result.Select((x, i) => CropBounds(x, i, result.Length)).ToArray();
            _renderer.Render(sanitizedSpaceToRender);

            UpdateBounds(result);

            return result;
        }

        protected abstract T[][,] GetEvolvedSpace(T[][,] overlappedSpace);

        private T[,] CropBounds(T[,] raw, int iteration, int iterationLimit)
        {
            if (iteration == 0)
            {
                return raw.SubArray(0, raw.GetLength(0) - 1, 0, raw.GetLength(1) - 2);
            }

            if (iteration == iterationLimit - 1)
            {
                return raw.SubArray(0, raw.GetLength(0) - 1, 1, raw.GetLength(1) - 1);
            }

            return raw.SubArray(0, raw.GetLength(0) - 1, 1, raw.GetLength(1) - 2);
        }

        // doesnt work with 3 cell width slices 
        private void UpdateBounds(T[][,] space)
        {
            if (space.Length == 1)
            {
                return;
            }

            for (int u = 0; u < space.Length; u++)
            {
                var chunk = space[u];
                var isFirstChunk = u == 0;
                var isLastChunk = u == space.Length - 1;

                if (!isFirstChunk && !isLastChunk)
                {
                    var nextChunk = space[u + 1];
                    var prevChunk = space[u - 1];

                    for (int i = 0; i < chunk.GetLength(0); i++)
                    {
                        chunk[i, 0] = prevChunk[i, prevChunk.GetLength(1) - 2];
                        chunk[i, chunk.GetLength(1) - 1] = nextChunk[i, 1];
                    }
                    continue;
                }

                if (isFirstChunk)
                {
                    var nextChunk = space[u + 1];

                    for (int i = 0; i < chunk.GetLength(0); i++)
                    {
                        chunk[i, chunk.GetLength(1) - 1] = nextChunk[i, 1];
                    }
                    continue;
                }

                if (isLastChunk)
                {
                    var prevChunk = space[u - 1];

                    for (int i = 0; i < chunk.GetLength(0); i++)
                    {
                        chunk[i, 0] = prevChunk[i, prevChunk.GetLength(1) - 2];
                    }
                }
            }
        }
    }
}
