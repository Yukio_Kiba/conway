﻿using GameOfLife.Engine.GameIterationCommands.Core;
using GameOfLife.Engine.Evolvers.Core;
using GameOfLife.Engine.Renderers.Core;

namespace GameOfLife.Engine.GameIterationCommands
{
    public class SolidSpaceGameIterationCommand<T> : IExecuteGameIterationCommand<T[,]>
    {
        private readonly IGameRenderer<T[,]> _renderer;
        private readonly IGameEvolver<T[,]> _gameEvolver;

        public SolidSpaceGameIterationCommand(
            IGameRenderer<T[,]> gameRenderer
            , IGameEvolver<T[,]> gameEvolver)
        {
            _renderer = gameRenderer;
            _gameEvolver = gameEvolver;
        }

        public T[,] Execute(T[,] space)
        {
            _renderer.Render(space);
            var newState = _gameEvolver.Evolve(space);

            return newState;
        }
    }
}
