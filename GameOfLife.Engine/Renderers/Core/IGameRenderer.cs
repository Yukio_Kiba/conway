﻿namespace GameOfLife.Engine.Renderers.Core
{
    public interface IGameRenderer<TSpace>
    {
        void Render(TSpace space);
    }
}
