﻿namespace GameOfLife.Engine.StateProviders.Core
{
    public interface ISpaceProvider<TSpace>
    {
        TSpace GetSpace();
    }
}
