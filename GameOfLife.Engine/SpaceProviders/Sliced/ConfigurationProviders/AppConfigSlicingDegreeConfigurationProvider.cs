﻿using System.Configuration;

namespace GameOfLife.Engine.SpaceProviders.Sliced.ConfigurationProviders
{
    public class AppConfigSlicingDegreeConfigurationProvider : ISlicingDegreeConfigurationProvider
    {
        public int GetSlicingDegree()
        {
            var raw = ConfigurationManager.AppSettings["slicingDegree"];

            return int.Parse(raw);
        }
    }
}
