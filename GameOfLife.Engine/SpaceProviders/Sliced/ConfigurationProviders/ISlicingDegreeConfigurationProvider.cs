﻿namespace GameOfLife.Engine.SpaceProviders.Sliced.ConfigurationProviders
{
    public interface ISlicingDegreeConfigurationProvider
    {
        int GetSlicingDegree();
    }
}
