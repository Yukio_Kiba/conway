﻿using GameOfLife.Engine.Extensions;
using GameOfLife.Engine.SpaceProviders.Sliced.ConfigurationProviders;
using GameOfLife.Engine.StateProviders.Core;
using System;
using System.Linq;

namespace GameOfLife.Engine.SpaceProviders.Sliced
{
    public class SlicedOverlappedBinaryRandomSpaceProvider : ISpaceProvider<bool[][,]>
    {
        private readonly ISpaceProvider<bool[,]> _solidSpaceProvider;
        private readonly int _slicingDegree;

        public SlicedOverlappedBinaryRandomSpaceProvider(
            ISpaceProvider<bool[,]> solidSpaceProvider
            , ISlicingDegreeConfigurationProvider slicingDegreeConfigurationProvider)
        {
            _solidSpaceProvider = solidSpaceProvider;
            _slicingDegree = slicingDegreeConfigurationProvider.GetSlicingDegree();
        }

        public bool[][,] GetSpace()
        {
            var solidSpace = _solidSpaceProvider.GetSpace();

            var sliced = SliceState(solidSpace);

            return sliced;
        }

        private bool[][,] SliceState(bool[,] initialState)
        {
            if (_slicingDegree == 1)
            {
                return new bool[][,]
                {
                    initialState
                };
            }

            var arrayWidth = initialState.GetLength(1);
            var sliceWidth = (int)Math.Floor((double)arrayWidth / _slicingDegree);

            var lastUpperBound = 0;
            var result = Enumerable.Range(0, _slicingDegree).Select(i =>
            {
                var lowerBound = (i == 0) ? lastUpperBound : lastUpperBound - 1;
                if (arrayWidth % _slicingDegree != 0
                    && (arrayWidth - i * sliceWidth) % (sliceWidth + 1) == 0
                    && i != 0 && i != _slicingDegree - 1)
                {
                    sliceWidth++;
                }

                var upperBound = (i == _slicingDegree - 1) ? lowerBound + sliceWidth : lastUpperBound + sliceWidth;

                lastUpperBound = upperBound;

                return initialState.SubArray(0, initialState.GetLength(0) - 1, lowerBound, upperBound);
            }).ToArray();

            return result;
        }

    }
}
