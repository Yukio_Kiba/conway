﻿using GameOfLife.Engine.StateProviders.Core;
using System;

namespace GameOfLife.Engine.SpaceProviders.Solid
{
    public class SolidBinaryRandomSpaceProvider : ISpaceProvider<bool[,]>
    {
        public bool[,] GetSpace()
        {
            var initialState = new bool[25, 45];
            Randomize(initialState);

            return initialState;
        }

        private void Randomize(bool[,] input)
        {
            var random = new Random();
            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    input[i, j] = random.Next() % 2 == 0;
                }
            }
        }
    }
}
