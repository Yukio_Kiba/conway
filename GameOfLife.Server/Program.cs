﻿using GameOfLife.Engine.Evolvers;
using System;

namespace GameOfLife.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var gameEvolver = new ConwayParallelEvolver();
                using (var server = new Server<bool[,]>(gameEvolver, int.Parse(args[0])))
                {
                    Console.WriteLine("Listening on port: " + args[0]);
                    server.Start();
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Please, specify in arguments port to listen on.");
                Console.ReadKey();
            }

        }
    }
}
