﻿using GameOfLife.Engine.Evolvers.Core;
using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameOfLife.Server
{
    public class Server<TSpace> : IDisposable
    {
        private readonly TcpListener _listener;
        private readonly IGameEvolver<TSpace> _gameEvolver;

        public Server(
            IGameEvolver<TSpace> gameEvolver
            , int port)
        {
            _gameEvolver = gameEvolver;

            var ipAddress = Dns.GetHostEntry("localhost").AddressList[1];
            _listener = new TcpListener(ipAddress, port);
        }

        public void Start()
        {
            _listener.Start();

            while (true)
            {
                var client = _listener.AcceptTcpClient();
                var stream = client.GetStream();

                var binaryFormatter = new BinaryFormatter();
                var deserializedMatrix = (TSpace)binaryFormatter.Deserialize(stream);

                var evolvedMatrix = _gameEvolver.Evolve(deserializedMatrix);
                binaryFormatter.Serialize(stream, evolvedMatrix);

                stream.Close();
            }
        }

        public void Dispose()
        {
            _listener.Stop();
        }
    }
}
